//
//  Token.swift
//  Entities
//
//  Created by Maxim Krouk on 2/19/20.
//

public struct Token {
    public var value: String
    public var type: String
    
    public init(value: String, type: String) {
        self.value = value
        self.type = type
    }
}
