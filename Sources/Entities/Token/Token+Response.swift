//
//  Token+Response.swift
//  Entities
//
//  Created by Maxim Krouk on 2/19/20.
//

import Foundation

extension Token {
    
    public struct Response: Decodable {
        public var accessToken: String
        public var tokenType: String
    }
    
    public init(from response: Response) {
        self.init(value: response.accessToken,
                  type: response.tokenType)
    }
    
}

extension Token.Response {
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.accessToken = try container.decode(String.self, forKey: .accessToken)
        self.tokenType = try container.decode(String.self, forKey: .tokenType)
    }
}
