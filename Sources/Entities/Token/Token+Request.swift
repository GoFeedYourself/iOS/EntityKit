//
//  Token+Request.swift
//  Entities
//
//  Created by Maxim Krouk on 2/19/20.
//

extension Token {
    
    public struct Request: Encodable {
        public var grantType: String?
        public var username: String
        public var password: String
        public var scope: String?
        public var clientId: String?
        public var clientSecret: String?
    }
    
}
